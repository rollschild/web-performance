# Understanding Web Performance

- reduce **latency**
- `HTTP/1.x` protocol has **head-of-line blocking** issues
  - browsers limit the number of requests it can make at a single time (typically, six)
- Areas to improve:
  - number of requests on a page
  - amount of data the page contains
  - amount of time it takes the page to load
- There will be differences in load times among devices for responsive websites
  - because of **media queries** which display differently at different widths
- On high DPI (dots per inch) displays, a higher-resolution set of images is needed than for standard DPI displays in order to retain high visual quality
- **Minification**
  - a process by which all whitespaces and unnecessary characters are stripped from a text-based asset without affecting the way that asset functions
-
